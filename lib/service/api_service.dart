import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:ram_pnc/model/failure.dart';
import 'package:ram_pnc/model/ram_character.dart';

class ApiService {
  const ApiService({required Dio dio}) : _dio = dio;

  final Dio _dio;

  Future<Either<Failure, RamCharacter>> fetchNewRandomCharacter(int id) async {
    try {
      final response = await _dio.get<Map<String, dynamic>>('https://rickandmortyapi.com/api/character/$id');

      final body = response.data;

      if (body == null) {
        return const Left(Failure());
      }

      final character = RamCharacter.fromJson(body);

      return Right(character);
    } catch (e) {
      return const Left(Failure());
    }
  }
}
