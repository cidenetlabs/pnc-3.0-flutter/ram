part of 'package:ram_pnc/cubit/ram_cubit.dart';

abstract class RamEvent extends Equatable {
  const RamEvent();

  const factory RamEvent.newRandomCharacterRequested() = NewRandomCharacterRequested;

  @override
  List<Object?> get props => [];
}

class NewRandomCharacterRequested extends RamEvent {
  const NewRandomCharacterRequested();
}
