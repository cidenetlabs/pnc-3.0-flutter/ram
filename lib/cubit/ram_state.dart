part of 'package:ram_pnc/cubit/ram_cubit.dart';

abstract class RamState extends Equatable {
  const RamState();

  const factory RamState.initialState() = Initial;
  const factory RamState.loading() = Loading;
  const factory RamState.success(RamCharacter character) = Success;
  const factory RamState.error() = Error;

  @override
  List<Object?> get props => [];
}

class Initial extends RamState {
  const Initial();
}

class Loading extends RamState {
  const Loading();
}

class Success extends RamState {
  const Success(this.character);

  final RamCharacter character;

  @override
  List<Object?> get props => [character];
}

class Error extends RamState {
  const Error();
}
