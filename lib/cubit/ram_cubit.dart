import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ram_pnc/model/ram_character.dart';
import 'package:ram_pnc/service/api_service.dart';

part 'package:ram_pnc/cubit/ram_state.dart';
part 'package:ram_pnc/cubit/ram_event.dart';

class RamCubit extends Bloc<RamEvent, RamState> {
  RamCubit({
    required ApiService apiService,
  })  : _apiService = apiService,
        super(const RamState.initialState()) {
    on<NewRandomCharacterRequested>(_fetchNewRandomCharacter);
  }

  final ApiService _apiService;

  Future<void> _fetchNewRandomCharacter(NewRandomCharacterRequested event, Emitter<RamState> emit) async {
    emit(const RamState.loading());

    final randomId = Random().nextInt(1100);

    final responseBox = await _apiService.fetchNewRandomCharacter(randomId);

    final response = responseBox.fold<Object>(
      (failure) => failure,
      (char) => char,
    );

    if (responseBox.isLeft()) {
      return emit(const RamState.error());
    }

    final character = response as RamCharacter;

    emit(RamState.success(character));
  }
}
