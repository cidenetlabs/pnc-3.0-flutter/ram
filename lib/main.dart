import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ram_pnc/bloc_observer.dart';
import 'package:ram_pnc/cubit/ram_cubit.dart';
import 'package:ram_pnc/service/api_service.dart';

void main() {
  BlocOverrides.runZoned(
    () => runApp(const RamApp()),
    blocObserver: AppBlocObserver(),
  );
}

class RamApp extends StatelessWidget {
  const RamApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rick and Morty',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider<RamCubit>(
        create: (context) => RamCubit(apiService: ApiService(dio: Dio())),
        child: const Dashboard(title: 'RaM Dashboard'),
      ),
    );
  }
}

class Dashboard extends StatelessWidget {
  const Dashboard({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: BlocListener<RamCubit, RamState>(
        listenWhen: (previous, current) => previous is! Error && current is Error,
        listener: (context, state) {
          ScaffoldMessenger.of(context)
            ..clearSnackBars()
            ..showSnackBar(
              const SnackBar(
                content: Text('Error!!!'),
              ),
            );
        },
        child: const PageContent(),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.search),
        onPressed: () {
          final cubit = BlocProvider.of<RamCubit>(context);
          cubit.add(const RamEvent.newRandomCharacterRequested());
        },
      ),
    );
  }
}

class PageContent extends StatelessWidget {
  const PageContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocBuilder<RamCubit, RamState>(
        builder: (context, state) {
          if (state is Success) {
            return Column(
              children: [
                Image.network(state.character.image),
                Text(state.character.name),
                Text(state.character.gender),
              ],
            );
          }

          if (state is Loading) {
            return const CircularProgressIndicator();
          }

          if (state is Error) {
            return const Text('Boom! #@%#');
          }

          return const Text('Idle');
        },
      ),
    );
  }
}
